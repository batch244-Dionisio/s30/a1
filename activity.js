// actvivity

// number 2
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "totalFruits"}
]);

// number 3 
db.fruits.aggregate([
    {$match: { stock: {$gte: 20}}},
    {$count: "totalFruits"}
]);

// number 4
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplierId", AveragePriceOnSale: {$avg: "$price"}}}
])


// number 5
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplierId", highestPriceOnsale: {$max: "$price"}}}
]);


// number 6
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplierId", lowestPriceOnSale: {$min: "$price"}}}
]);
